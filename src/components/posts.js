import React from 'react';


class Posts extends React.Component {
  constructor(props) {
    super(props);
    this.state = { data: [], title: '' };
  }

  componentDidMount() {
    fetch('https://jsonplaceholder.typicode.com/posts')
      .then((res) => res.json())
      .then((result) => {
        this.setState({ data: result });
      });
  }

  updateTitle = (id) => {
    const title = document.querySelector(`.title_${id}`).textContent
    fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
      method: 'PUT',
      body: JSON.stringify({
        id: id,
        title: title,
      }),
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
      },
    }).then((response) => response.status === 200 ? window.alert("Successfully updated.") : null)
        .then(() => this.setState(prevState => ({
      data: prevState.data.map(el => el.id === id ? {...el, title: title} : el)
    })))
  }

  deleteBody = (id) => {
    fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
      method: 'DELETE',
    }).then((response) => response.status === 200 ? window.alert("Successfully deleted.") : null)
        .then(() => this.setState(prevState => ({
      data: prevState.data.map(el => el.id === id ? {...el, body: null} : el)
    })))
  }

  render() {
    const items = this.state.data;

    return (
      <div >
          {items.map((item) => {
            return (
                <div className='container mtb-3'>
                <div className='table-responsive'>
                  <table className='table'>
                  <thead>
                  <tr>
                    <th>id:</th>
                    <th>
                      title:
                    <button onClick={ () => this.updateTitle(item.id)}>edit</button>
                    </th>
                    <th>body:
                      <button onClick={ () => this.deleteBody(item.id)}>delete</button>
                    </th>
                  </tr>
                  </thead>
                  <tbody>
                  <tr>
                    <td>{item.id}</td>
                    <td contenteditable="true" className={`title_${item.id}`}>{item.title}</td>
                    <td>{item.body}</td>
                  </tr>
                  </tbody>
                </table>
                </div></div>
            );
          })}
      </div>
    );
  }
}

export default Posts;


// <form >
//   <li>
//     <p>user id: {item.userId}</p>
//     <br/>
//     <p>title: {item.title}</p>
//     <br/>
//     <p>body: {item.body}</p>
//     <br/>
//   </li>
// </form>